package ffuf.playground.repositories.ramen

import de.ffuf.pass.common.repositories.PassRepository
import ffuf.playground.spec.dbo.ramen.RamenRamen
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface RamenRamenRepository : PassRepository<RamenRamen, Long> {
    @Query("SELECT t from RamenRamen t WHERE t.id > :firstResultId ORDER BY t.id ASC")
    fun findAllWithFirstResultId(firstResultId: Long, pageable: Pageable): Page<RamenRamen>

    @Query("SELECT t from RamenRamen t WHERE t.id <= :lastResultId ORDER BY t.id ASC")
    fun findAllWithLastResultId(lastResultId: Long, pageable: Pageable): Page<RamenRamen>
}
