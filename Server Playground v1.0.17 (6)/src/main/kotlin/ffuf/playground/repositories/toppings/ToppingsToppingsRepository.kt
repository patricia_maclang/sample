package ffuf.playground.repositories.toppings

import de.ffuf.pass.common.repositories.PassRepository
import ffuf.playground.spec.dbo.toppings.ToppingsToppings
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ToppingsToppingsRepository : PassRepository<ToppingsToppings, Long> {
    @Query("SELECT t from ToppingsToppings t WHERE t.id > :firstResultId ORDER BY t.id ASC")
    fun findAllWithFirstResultId(firstResultId: Long, pageable: Pageable): Page<ToppingsToppings>

    @Query("SELECT t from ToppingsToppings t WHERE t.id <= :lastResultId ORDER BY t.id ASC")
    fun findAllWithLastResultId(lastResultId: Long, pageable: Pageable): Page<ToppingsToppings>

    @Query(
        "SELECT t from ToppingsToppings t LEFT JOIN FETCH t.ramenToppings",
        countQuery = "SELECT count(id) FROM ToppingsToppings"
    )
    fun findAllWithRamenToppings(pageable: Pageable): Page<ToppingsToppings>
}
