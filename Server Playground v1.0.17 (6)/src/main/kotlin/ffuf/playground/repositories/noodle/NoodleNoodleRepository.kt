package ffuf.playground.repositories.noodle

import de.ffuf.pass.common.repositories.PassRepository
import ffuf.playground.spec.dbo.noodle.NoodleNoodle
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface NoodleNoodleRepository : PassRepository<NoodleNoodle, Long> {
    @Query("SELECT t from NoodleNoodle t WHERE t.id > :firstResultId ORDER BY t.id ASC")
    fun findAllWithFirstResultId(firstResultId: Long, pageable: Pageable): Page<NoodleNoodle>

    @Query("SELECT t from NoodleNoodle t WHERE t.id <= :lastResultId ORDER BY t.id ASC")
    fun findAllWithLastResultId(lastResultId: Long, pageable: Pageable): Page<NoodleNoodle>
}
