package ffuf.playground.repositories.soupcategory

import de.ffuf.pass.common.repositories.PassRepository
import ffuf.playground.spec.dbo.soupcategory.SoupcategorySoupcategory
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface SoupcategorySoupcategoryRepository : PassRepository<SoupcategorySoupcategory, Long> {
    @Query("SELECT t from SoupcategorySoupcategory t WHERE t.id > :firstResultId ORDER BY t.id ASC")
    fun findAllWithFirstResultId(firstResultId: Long, pageable: Pageable):
            Page<SoupcategorySoupcategory>

    @Query("SELECT t from SoupcategorySoupcategory t WHERE t.id <= :lastResultId ORDER BY t.id ASC")
    fun findAllWithLastResultId(lastResultId: Long, pageable: Pageable):
            Page<SoupcategorySoupcategory>
}
