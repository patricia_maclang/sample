package ffuf.playground.spec.handler.ramen

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassHandler
import de.ffuf.pass.common.utilities.extensions.queryParamOrNull
import de.ffuf.pass.common.utilities.extensions.toDate
import de.ffuf.pass.common.utilities.extensions.toOffsetDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.function.body
import org.springframework.web.servlet.function.paramOrNull

interface RamenRamenDatabaseHandler

@Component("ramen.Ramen")
class RamenRamenHandler : PassHandler() {
    @Autowired
    lateinit var databaseHandler: RamenRamenDatabaseHandler

    @Autowired
    private lateinit var objectMapper: ObjectMapper
}
