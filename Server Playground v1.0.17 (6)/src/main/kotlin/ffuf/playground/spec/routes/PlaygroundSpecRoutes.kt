@file:Suppress("WildcardImport")

package ffuf.playground.spec.routes

import de.ffuf.pass.common.config.ContentNegotiationJsonHandler
import kotlin.Suppress
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.function.router

@Configuration
class PlaygroundSpecRoutes
