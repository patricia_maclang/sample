package ffuf.playground.spec.dbo.noodle

import de.ffuf.pass.common.models.PassModel
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode

@Entity(name = "NoodleNoodle")
@Table(name = "noodle_noodle")
data class NoodleNoodle(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    @Column(nullable = false)
    @Lob
    val name: String = "",
    @Column(nullable = false)
    @Lob
    val description: String = ""
) : PassModel<NoodleNoodle, Long>() {
    companion object {
        const val TEXTURE_S: String = "soft"

        const val TEXTURE_H: String = "hard"

        const val TEXTURE_N: String = "normal"
    }
}
