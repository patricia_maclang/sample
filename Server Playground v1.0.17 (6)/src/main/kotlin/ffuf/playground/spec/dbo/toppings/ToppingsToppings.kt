package ffuf.playground.spec.dbo.toppings

import de.ffuf.pass.common.models.PassModel
import ffuf.playground.spec.dbo.ramen.RamenRamen
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode

@Entity(name = "ToppingsToppings")
@Table(name = "toppings_toppings")
data class ToppingsToppings(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    @Column(nullable = false)
    @Lob
    val name: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val ramenToppings: RamenRamen? = null
) : PassModel<ToppingsToppings, Long>()
