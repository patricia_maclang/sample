package ffuf.playground.spec.dbo.soupcategory

import de.ffuf.pass.common.models.PassModel
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode

@Entity(name = "SoupcategorySoupcategory")
@Table(
    name = "soupcategory_soupcategory",
    uniqueConstraints = [
        UniqueConstraint(name = "UNIQ_5e237e06", columnNames = ["name"])
    ]

)
data class SoupcategorySoupcategory(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    @Column(nullable = false)
    @Lob
    val name: String = ""
) : PassModel<SoupcategorySoupcategory, Long>()
