package ffuf.playground.spec.dbo.ramen

import de.ffuf.pass.common.models.PassModel
import ffuf.playground.spec.dbo.noodle.NoodleNoodle
import ffuf.playground.spec.dbo.soup.SoupSoup
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode

@Entity(name = "RamenRamen")
@Table(name = "ramen_ramen")
data class RamenRamen(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Sample: ramen nagi
     */
    @ColumnDefault("'rameen'")
    @Column(nullable = false)
    @Lob
    val name: String = "rameen",
    /**
     * Sample: masarap
     */
    @Column(nullable = false)
    @Lob
    val description: String = "",
    @OneToOne(fetch = FetchType.LAZY)
    val ramenNoodle: NoodleNoodle? = null,
    @OneToOne(fetch = FetchType.LAZY)
    val ramenSoup: SoupSoup? = null
) : PassModel<RamenRamen, Long>()
