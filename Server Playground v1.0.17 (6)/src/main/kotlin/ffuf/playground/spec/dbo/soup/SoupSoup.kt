package ffuf.playground.spec.dbo.soup

import de.ffuf.pass.common.models.PassModel
import ffuf.playground.spec.dbo.soupcategory.SoupcategorySoupcategory
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode

@Entity(name = "SoupSoup")
@Table(
    name = "soup_soup",
    uniqueConstraints = [
        UniqueConstraint(name = "UNIQ_5e237e06", columnNames = ["name"])
    ]

)
data class SoupSoup(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Sample: Soup Number 5
     */
    @Column(nullable = false)
    @Lob
    val name: String = "",
    @Column(nullable = false)
    @Lob
    val description: String = "",
    @OneToOne(fetch = FetchType.LAZY)
    val soupSoupcategory: SoupcategorySoupcategory? = null
) : PassModel<SoupSoup, Long>()
