package ffuf.playground.handlerimpl.ramen

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffuf.playground.repositories.ramen.RamenRamenRepository
import ffuf.playground.spec.dbo.ramen.RamenRamen
import ffuf.playground.spec.handler.ramen.RamenRamenDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("ramen.RamenRamenHandler")
class RamenRamenHandlerImpl : PassDatabaseHandler<RamenRamen, RamenRamenRepository>(),
        RamenRamenDatabaseHandler
