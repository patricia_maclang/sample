package ffuf.playground.handlerimpl.noodle

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffuf.playground.repositories.noodle.NoodleNoodleRepository
import ffuf.playground.spec.dbo.noodle.NoodleNoodle
import ffuf.playground.spec.handler.noodle.NoodleNoodleDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("noodle.NoodleNoodleHandler")
class NoodleNoodleHandlerImpl : PassDatabaseHandler<NoodleNoodle, NoodleNoodleRepository>(),
        NoodleNoodleDatabaseHandler
