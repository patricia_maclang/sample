package ffuf.playground.handlerimpl.soup

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffuf.playground.repositories.soup.SoupSoupRepository
import ffuf.playground.spec.dbo.soup.SoupSoup
import ffuf.playground.spec.handler.soup.SoupSoupDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("soup.SoupSoupHandler")
class SoupSoupHandlerImpl : PassDatabaseHandler<SoupSoup, SoupSoupRepository>(),
        SoupSoupDatabaseHandler
