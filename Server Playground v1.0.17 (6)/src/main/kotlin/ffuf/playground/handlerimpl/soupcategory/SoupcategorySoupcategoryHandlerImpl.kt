package ffuf.playground.handlerimpl.soupcategory

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffuf.playground.repositories.soupcategory.SoupcategorySoupcategoryRepository
import ffuf.playground.spec.dbo.soupcategory.SoupcategorySoupcategory
import ffuf.playground.spec.handler.soupcategory.SoupcategorySoupcategoryDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("soupcategory.SoupcategorySoupcategoryHandler")
class SoupcategorySoupcategoryHandlerImpl : PassDatabaseHandler<SoupcategorySoupcategory,
        SoupcategorySoupcategoryRepository>(), SoupcategorySoupcategoryDatabaseHandler
