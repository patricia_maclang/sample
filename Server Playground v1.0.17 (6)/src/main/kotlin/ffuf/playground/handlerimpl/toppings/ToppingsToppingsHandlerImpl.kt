package ffuf.playground.handlerimpl.toppings

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffuf.playground.repositories.toppings.ToppingsToppingsRepository
import ffuf.playground.spec.dbo.toppings.ToppingsToppings
import ffuf.playground.spec.handler.toppings.ToppingsToppingsDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("toppings.ToppingsToppingsHandler")
class ToppingsToppingsHandlerImpl : PassDatabaseHandler<ToppingsToppings,
        ToppingsToppingsRepository>(), ToppingsToppingsDatabaseHandler
