import lenala.azure.gradle.webapp.AzureWebAppExtension
import lenala.azure.gradle.webapp.DeployTask
import lenala.azure.gradle.webapp.configuration.AppService
import lenala.azure.gradle.webapp.configuration.AppServiceType
import lenala.azure.gradle.webapp.configuration.Deployment
import lenala.azure.gradle.webapp.configuration.DeploymentType
import lenala.azure.gradle.webapp.model.PricingTierEnum
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.3.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.3.71"
    kotlin("plugin.spring") version "1.3.71"
    kotlin("plugin.allopen") version "1.3.71"
    kotlin("plugin.noarg") version "1.3.71"
    id("war")
    id("org.jmailen.kotlinter") version "2.1.3"
    id("io.gitlab.arturbosch.detekt") version "1.2.1"
    `maven-publish`
    jacoco
    id("de.ffuf.pass.gradle") version "1.0.11"
    id("lenala.azure.azurewebapp") version "1.0.4"
    id("org.liquibase.gradle") version "2.0.1"
}

val SPEC_VERSION = "1.0.20.22551"

description = "Playground"
group = "ffuf.playground"
version = "$SPEC_VERSION." + (if (project.hasProperty("patchVersion")) project.property("patchVersion") else "0")
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    val azureArtifactsGradleAccessToken: String? by project
    mavenLocal()
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://pkgs.dev.azure.com/ffuf/_packaging/Pass/maven/v1")
        credentials {
            username = "AZURE_ARTIFACTS"
            password = System.getenv("AZURE_ARTIFACTS_ENV_ACCESS_TOKEN_2") ?: azureArtifactsGradleAccessToken
        }
    }
}
val antJUnit by configurations.getting
dependencies {

    api(group = "de.ffuf.pass", name = "passcommon", version = "2.0.39")
    api(group = "ffuf.playground.spec", name = "playgroundspec", version = SPEC_VERSION)
    implementation("org.springframework.boot:spring-boot-starter-actuator")

    runtimeOnly("com.h2database:h2") // only for local testing

    // UNIT TESTS
    testApi(group = "de.ffuf.pass", name = "PassTestCommon", version = "2.0.39")

    antJUnit("org.apache.ant", "ant-junit", "1.10.5")

    liquibaseRuntime("org.liquibase:liquibase-core:3.8.0")
    liquibaseRuntime("org.liquibase:liquibase-groovy-dsl:2.0.1")
    liquibaseRuntime("ch.qos.logback:logback-core:1.2.3")
    liquibaseRuntime("ch.qos.logback:logback-classic:1.2.3")
    liquibaseRuntime(group = "com.microsoft.sqlserver", name = "mssql-jdbc")
}

liquibase {
    activities {
        create("diffMain") {
            // USE DOCKER: docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu ###
            (this.arguments as MutableMap<String, String>).apply {
                this["changeLogFile"] = "src/main/resources/db/changelog/db.changelog-diff.xml"
                this["url"] = "jdbc:sqlserver://localhost;databaseName=master"
                this["username"] = "sa"
                this["password"] = "yourStrong(!)Password"
// set e.g. the Dev Database to perform diffs
                this["referenceUrl"] = ""
                this["referenceUsername"] = ""
                this["referencePassword"] = ""
            }
        }
    }
}

allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.MappedSuperclass")
    annotation("javax.persistence.Embeddable")
}

tasks.withType<KotlinCompile> {

    sourceCompatibility = JavaVersion.VERSION_1_8.toString()
    targetCompatibility = JavaVersion.VERSION_1_8.toString()

    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=compatibility")
        jvmTarget = "1.8"
    }

}

configurations.all {
    exclude(module = "jakarta.validation-api")
    exclude(module = "hibernate-validator")
}

tasks.withType<DeployTask> {
    dependsOn("bootJar")
}

configure<AzureWebAppExtension> {
    appName = "app"
    appService = AppService().apply {
        type = AppServiceType.WINDOWS
        setJavaWebContainer("tomcat 8.5")
        setJavaVersion("1.8.0_181")
    }

    deployment = Deployment().apply {
        type = DeploymentType.JAR
    }
}
